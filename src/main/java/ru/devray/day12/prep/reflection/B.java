package ru.devray.day12.prep.reflection;

public class B extends A{
    @Override
    public void doImportantStuff() {
        System.out.println("from class B");
    }

    public static void main(String[] args) {
        B b = new B();
        b.doImportantStuff();

        int i = veryOldMethodThatWillBeRemoved();
//        Integer i = new Integer(89);

    }

    @Deprecated
    public static int veryOldMethodThatWillBeRemoved() {
        return 1;
    }
}

package ru.devray.day12.prep.reflection;

public class Secret {

    public int countBunnies = 90;

    private int secretNonStaticField = 123;
    private static int secretStaticField = 333;

    private static void secretStaticMethod() {
        System.out.println("!!!static secrets!!!");
    }

    private void secretNonStaticMethod() {
        System.out.println("!!!non-static secrets!!!");
    }
    private void secretNonStaticMethod(Object o, String s1) {
        System.out.println("!!!non-static secrets!!! with OBJECT and STRING");
    }

    public Secret(){}

    public Secret(int countBunnies) {
        this.countBunnies = countBunnies;
    }

    public Secret(int countBunnies, int secretNonStaticField) {
        this.countBunnies = countBunnies;
        this.secretNonStaticField = secretNonStaticField;
    }



    @Override //
    public String toString() {
        return "Secret{" +
                "countBunnies=" + countBunnies +
                ", secretNonStaticField=" + secretNonStaticField +
                '}';
    }
}

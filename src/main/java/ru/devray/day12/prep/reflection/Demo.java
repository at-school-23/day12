package ru.devray.day12.prep.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Demo {

    public static void main(String[] args) {
        Secret secret = new Secret();
//        secret.secretNonStaticMethod();
        Method[] methods = Secret.class.getDeclaredMethods();
        Field[] declaredFields = Secret.class.getDeclaredFields();

        System.out.println(Arrays.toString(methods));
        System.out.println(Arrays.toString(declaredFields));

        //вызов приватного метода через рефлексию
//        secret.getClass().getDeclaredMethod("secretNonStaticMethod", Object.class); //(Object o)
        try {
            Method secretNonStaticMethod = secret.getClass().getDeclaredMethod("secretNonStaticMethod", Object.class, String.class);// ()
            secretNonStaticMethod.setAccessible(true);
            secretNonStaticMethod.invoke(secret, new Object(), "12erfdfs");
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }

        //вызов приватного метода через рефлексию
        try {
            Field secretNonStaticField = secret.getClass().getDeclaredField("secretNonStaticField");
            secretNonStaticField.setAccessible(true);
            secretNonStaticField.set(secret, 456);
            System.out.println(secretNonStaticField.get(secret));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        System.out.println(Arrays.toString(Secret.class.getDeclaredConstructors()));
    }
}

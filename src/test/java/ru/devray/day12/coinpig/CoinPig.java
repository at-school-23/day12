package ru.devray.day12.coinpig;

import java.util.ArrayList;
import java.util.List;

public class CoinPig {

    List<Coin> storage = new ArrayList<>();

    public void addCoin(Coin coin) {
        storage.add(coin);
    }

    public int getCoinAmount() {
        return storage.size();
    }

    public double getCoinSum() {
        return storage.stream().mapToDouble(c -> c.getValue()).sum();
    }
}

package ru.devray.day12.coinpig;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CoinPigTest {
    @Test
    public void testAddCoin() {
        Coin coin = new Coin(0.25);
        CoinPig coinPig = new CoinPig();
        coinPig.addCoin(coin);

        Assert.assertEquals(coinPig.getCoinAmount(),1);
    }

    @Test
    public void testCoinSum() {
        Coin coin1 = new Coin(0.05);
        Coin coin2 = new Coin(5);

        CoinPig coinPig = new CoinPig();
        coinPig.addCoin(coin1);
        coinPig.addCoin(coin2);

        double coinSum = coinPig.getCoinSum();

        Assert.assertEquals(coinSum, 5.05);
    }
}

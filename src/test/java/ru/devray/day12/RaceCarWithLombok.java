package ru.devray.day12;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;
//DTO
@Data
@AllArgsConstructor
public class RaceCarWithLombok {
    int crew;
    int wheels;
    String brand;
}

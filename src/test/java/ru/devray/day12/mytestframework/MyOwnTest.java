package ru.devray.day12.mytestframework;

import org.testng.Assert;

public class MyOwnTest {

    @MyTest
    public void myFirstTest() {
        System.out.println("Test started!");
        int result = 2 + getMyData();
        Assert.assertEquals(result, 4);
    }

    @MyTest
    public void myFirst2Test() {
        System.out.println("Test started!");
        int result = 2 + getMyData();
        Assert.assertEquals(result, 4);
    }

    public int getMyData() {
        return 2;
    }

    public int getMyData2() {
        return 2;
    }
}

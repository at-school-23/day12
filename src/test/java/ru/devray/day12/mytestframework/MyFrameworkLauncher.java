package ru.devray.day12.mytestframework;

import lombok.SneakyThrows;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class MyFrameworkLauncher {
    @SneakyThrows
    public static void main(String[] args) {
        MyOwnTest myOwnTest = new MyOwnTest();
        Method[] declaredMethods = myOwnTest.getClass().getDeclaredMethods();

        for (Method method : declaredMethods) {
            Annotation[] declaredAnnotations = method.getDeclaredAnnotations();
            for (Annotation annotation : declaredAnnotations) {
                if (annotation instanceof MyTest) {
                    method.setAccessible(true);
                    method.invoke(myOwnTest);
                }
            }
        }
    }
}

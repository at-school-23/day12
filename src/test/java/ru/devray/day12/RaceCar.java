package ru.devray.day12;

import java.util.Objects;

public class RaceCar {
    int crew;
    int wheels;
    String brand;

    public RaceCar(int crew, int wheels, String brand) {
        this.crew = crew;
        this.wheels = wheels;
        this.brand = brand;
    }

    public int getCrew() {
        return crew;
    }

    public void setCrew(int crew) {
        this.crew = crew;
    }

    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RaceCar raceCar = (RaceCar) o;
        return crew == raceCar.crew && wheels == raceCar.wheels && Objects.equals(brand, raceCar.brand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(crew, wheels, brand);
    }

    @Override
    public String toString() {
        return "RaceCar{" +
                "crew=" + crew +
                ", wheels=" + wheels +
                ", brand='" + brand + '\'' +
                '}';
    }
}
